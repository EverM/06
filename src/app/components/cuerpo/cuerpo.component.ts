import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cuerpo',
  templateUrl: './cuerpo.component.html',
  styleUrls: ['./cuerpo.component.css'],
})
export class CuerpoComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
  ganador!: boolean;
  mensaje!: string;
  contador1 = 0;
  contador2 = 0;
  posiciones = [
    ['-', '-', '-'],
    ['-', '-', '-'],
    ['-', '-', '-'],
  ];
  jugador = 'X';

  presion(fila: number, columna: number) {
    if (this.posiciones[fila][columna] == '-') {
      this.posiciones[fila][columna] = this.jugador;
      this.verificarGanador('X');
      this.verificarGanador('O');
      this.cambioFicha();
    }
  }

  nuevoJuego() {
    for (let i = 0; i < 3; i++)
      for (let j = 0; j < 3; j++) this.posiciones[i][j] = '-';
  }

  cambioFicha() {
    if (this.jugador == 'X') this.jugador = 'O';
    else this.jugador = 'X';
  }

  verificarGanador(ficha: string) {
    if (
      this.posiciones[0][0] == ficha &&
      this.posiciones[0][1] == ficha &&
      this.posiciones[0][2] == ficha
    ) {
      this.mensaje = `Ganador: ${this.jugador}`;
      this.ganador = true;
      if (this.ganador == true && this.jugador == 'X') {
        this.contador1++;
      } else if (this.ganador == true && this.jugador == 'O') {
        this.contador2++;
      }
    }

    if (
      this.posiciones[1][0] == ficha &&
      this.posiciones[1][1] == ficha &&
      this.posiciones[1][2] == ficha
    ) {
      this.mensaje = `Ganador: ${this.jugador}`;
      this.ganador = true;
      if (this.ganador == true && this.jugador == 'X') {
        this.contador1++;
      } else if (this.ganador == true && this.jugador == 'O') {
        this.contador2++;
      }
    }
    if (
      this.posiciones[2][0] == ficha &&
      this.posiciones[2][1] == ficha &&
      this.posiciones[2][2] == ficha
    ) {
      this.mensaje = `Ganador: ${this.jugador}`;
      this.ganador = true;
      if (this.ganador == true && this.jugador == 'X') {
        this.contador1++;
      } else if (this.ganador == true && this.jugador == 'O') {
        this.contador2++;
      }
    }
    if (
      this.posiciones[0][0] == ficha &&
      this.posiciones[1][0] == ficha &&
      this.posiciones[2][0] == ficha
    ) {
      this.mensaje = `Ganador: ${this.jugador}`;
      this.ganador = true;
      if (this.ganador == true && this.jugador == 'X') {
        this.contador1++;
      } else if (this.ganador == true && this.jugador == 'O') {
        this.contador2++;
      }
    }
    if (
      this.posiciones[0][1] == ficha &&
      this.posiciones[1][1] == ficha &&
      this.posiciones[2][1] == ficha
    ) {
      this.mensaje = `Ganador: ${this.jugador}`;
      this.ganador = true;
      if (this.ganador == true && this.jugador == 'X') {
        this.contador1++;
      } else if (this.ganador == true && this.jugador == 'O') {
        this.contador2++;
      }
    }
    if (
      this.posiciones[0][2] == ficha &&
      this.posiciones[1][2] == ficha &&
      this.posiciones[2][2] == ficha
    ) {
      this.mensaje = `Ganador: ${this.jugador}`;
      this.ganador = true;
      if (this.ganador == true && this.jugador == 'X') {
        this.contador1++;
      } else if (this.ganador == true && this.jugador == 'O') {
        this.contador2++;
      }
    }
    if (
      this.posiciones[0][0] == ficha &&
      this.posiciones[1][1] == ficha &&
      this.posiciones[2][2] == ficha
    ) {
      this.mensaje = `Ganador: ${this.jugador}`;
      this.ganador = true;
      if (this.ganador == true && this.jugador == 'X') {
        this.contador1++;
      } else if (this.ganador == true && this.jugador == 'O') {
        this.contador2++;
      }
    }
    if (
      this.posiciones[0][2] == ficha &&
      this.posiciones[1][1] == ficha &&
      this.posiciones[2][0] == ficha
    ) {
      this.mensaje = `Ganador: ${this.jugador}`;
      this.ganador = true;
      if (this.ganador == true && this.jugador == 'X') {
        this.contador1++;
      } else if (this.ganador == true && this.jugador == 'O') {
        this.contador2++;
      }
    }
  }


  }

